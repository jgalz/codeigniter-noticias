<?php

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();

        //Carregando o News_model
        //Model encarregado por retornar os dados da tabela news
        $this->load->model('news_model');

        //Carregando o helper de URL
        $this->load->helper('url_helper');
    }

    //Quando acessar o index, retorna todos as noticias do banco de dados
    public function index() {
        //Todas as noticias
        $data['news'] = $this->news_model->get_news();
        $data['title'] = 'Todas as notícias';

        //Carrega o header
        $this->load->view('templates/header', $data);
        //Carrega o view
        $this->load->view('news/index', $data);
        //Carrega o footer
        $this->load->view('templates/footer', $data);

    }

    //Ação que carrega apenas 1 noticia
    public function view($uri) {

        $data['news_item'] = $this->news_model->get_news($uri);

        if(empty($data['news_item'])) {
            show_404();
        }

        $data['title'] = $data['news_item']['title'];

        //Carrega o header
        $this->load->view('templates/header', $data);
        //Carrega o view
        $this->load->view('news/view', $data);
        //Carrega o footer
        $this->load->view('templates/footer', $data);

    }

    public function create() {
        //Ajudador de Formulario
        $this->load->helper('form');
        //Livraria de Validação de Formulario
        $this->load->library('form_validation');

        $data['title'] = 'Crie uma nova notícia.';

        //Setando os campos obrigatorios
        $this->form_validation->set_rules('title', 'Titulo', 'required');
        $this->form_validation->set_rules('text', 'Texto', 'required');

        //Se não receber nada
        if($this->form_validation->run() === false) {
            $this->load->view('templates/header', $data);
            $this->load->view('news/create');
            $this->load->view('templates/footer');
        } else {
            //Se receber e estiver preenchido todos os campos obrigatorios
            //Noticia adicionada
            $this->news_model->set_news();
            $this->load->view('news/success');
        }

    }









}
