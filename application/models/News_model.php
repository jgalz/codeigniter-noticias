<?php 

class News_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }


    //Se nenhum registro for passado como parameotr
    //Retorna todos os dados da tabela
    public function get_news($uri = false) {

        //Retorna todos os dados da tabela
        if($uri === false) {
           
            //Pegar todos os registros da tabela news
            $query = $this->db->get('news');
            //Vai retornar todos os resultados em array
            return $query->result_array();

        } else {
            //Se algum parâmetro for passado, vai retornar apenas ele
            $query = $this->db->get_where('news', array('uri'=>$uri));
            return $query->row_array();

        }       

    }

    public function set_news() {
        //Ajudador de URL
        $this->load->helper('url');
        $uri = url_title($this->input->post('title'), 'dash', TRUE);

        //Dados
        $data = array(
            'title' => $this->input->post('title'),
            'uri' => $uri,
            'text' => $this->input->post('text')
        );

        //Inserindo no banc ode dados    
        $this->db->insert('news', $data);

    }

}
