<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    //Actions para acessar as paginas estaticas
    public function view($page = 'home') {

        //Carregar todas as paginas estaticas do sistema

        //Verificação pra saber se existe a pagina estatica
        if(!file_exists(APPPATH.'views/pages/'.$page.'.php') ) {
            show_404();
        }

        //Pega primeira letra da string e deixa Maiuscula
        //Titulo
        $data['title'] = ucfirst($page);


        //Carrega o header
        $this->load->view('templates/header', $data);
        //Carrega o view
        $this->load->view('pages/'.$page, $data);
        //Carrega o footer
        $this->load->view('templates/footer', $data);

    }
}
